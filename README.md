# Hangaroo (Javascript Game)

> A simple single page app using React, created via create-react-app.  
> This is a redo of a popular desktop game called Hangaroo.  
> Player tries to guess the correct answer by choosing a letter per turn.  
> Player wins if all letters in the answer are revealed.  
> Player loses if 4 red-x (mistakes) were accumulated.  

## Build Setup

To run the app:

``` bash
# install dependencies
npm install

# serve
npm start
```

For detailed explanation on how things work, consult the [official React documentation](https://reactjs.org/docs/getting-started.html).

## Contact Information

Feel free to reach out to and connect with the instructor for any inquiries or problems with the sample project.

**PeeJay Saracho**  
0915 980 1701  
uplift.pjsaracho@gmail.com  
