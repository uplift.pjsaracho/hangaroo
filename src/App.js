import React from 'react';
import cross from './images/cross.png';
import redx from './images/redx.png';
import audio from './fail-buzzer.mp3';
import './App.css';

class App extends React.Component {
  state = {
    answer: [
      { letter: "J", revealed: false },
      { letter: "A", revealed: false },
      { letter: "V", revealed: false },
      { letter: "A", revealed: false },
      { letter: "S", revealed: false },
      { letter: "C", revealed: false },
      { letter: "R", revealed: false },
      { letter: "I", revealed: false },
      { letter: "P", revealed: false },
      { letter: "T", revealed: false },
    ],
    mistakeCounter: 0,
    alphabet : [
      { letter: "A", disabled: false },
      { letter: "B", disabled: false },
      { letter: "C", disabled: false },
      { letter: "D", disabled: false },
      { letter: "E", disabled: false },
      { letter: "F", disabled: false },
      { letter: "G", disabled: false },
      { letter: "H", disabled: false },
      { letter: "I", disabled: false },
      { letter: "J", disabled: false },
      { letter: "K", disabled: false },
      { letter: "L", disabled: false },
      { letter: "M", disabled: false },
      { letter: "N", disabled: false },
      { letter: "O", disabled: false },
      { letter: "P", disabled: false },
      { letter: "Q", disabled: false },
      { letter: "R", disabled: false },
      { letter: "S", disabled: false },
      { letter: "T", disabled: false },
      { letter: "U", disabled: false },
      { letter: "V", disabled: false },
      { letter: "W", disabled: false },
      { letter: "X", disabled: false },
      { letter: "Y", disabled: false },
      { letter: "Z", disabled: false },
    ],
    gameover: false,
  }


  alphabetClickHandler = (event) => {
    let correct = false;
    let updatedAnswer = this.state.answer.map( function(obj) {
      if(obj.letter === event.target.innerHTML) {
        obj.revealed = true;
        correct = true;
    }
      return obj;
    });

    let updatedAlphabet = this.state.alphabet.map( function(obj) {
      if(obj.letter === event.target.innerHTML)
        obj.disabled = true;
      return obj;
    });

    let mistake = this.state.mistakeCounter;
    if(!correct) {
      document.querySelector("#audio").play();
      mistake++;
    }

    this.setState({
      answer: updatedAnswer,
      mistakeCounter : mistake,
      alphabet: updatedAlphabet,
      gameover: mistake>=4
    });
  }

  // createLetterButtons = () => {
  //   let btns = [];
  //   for(let n=0; n<26; n++) {
  //     let letter = String.fromCharCode(65+n); 
  //     btns.push(
  //       <div id={letter}>{letter}</div>
  //     );
  //   }
  //   return btns;
  // } 

  render() {
    console.log(this.state.gameover)
    let displayLetters = this.state.alphabet.map( (l) => {
      if(l.disabled)
        return <div key={l.letter} className="disabled" onClick={this.alphabetClickHandler}>{l.letter}</div>
      else
        return <div key={l.letter} onClick={this.alphabetClickHandler}>{l.letter}</div>
    });

    let answerBoxes = this.state.answer.map( (element, index) => {
      if(element.revealed === true) {
        return <div key={index}>{element.letter}</div>
      } else {
        return <div key={index}></div>
      }
    });

    let mistakeBoxes = () => {
      let result = [];
      for(let n=0; n<4; n++) {
        if(n<this.state.mistakeCounter)
          result.push(<img key={n} src={redx} width="50px" alt="redx" />)
        else
          result.push(<img key={n} src={cross} width="50px" alt="cross" />)
      }
      return result;
    } 

    return (
      <div className="App">
        <header className="App-header">
          <div id="mistakeTracker">
            { mistakeBoxes() }
          </div>
          <div id="buttons">
            { displayLetters }
          </div>
          <div id="category">CATEGORY</div>
          <div id="answer">
            { answerBoxes }
          </div>
          <audio id="audio">
            <source src={audio} type="audio/mpeg" />
          </audio>
          {
            this.state.gameover &&
            <div className="modal">
              <div className="modal-content"></div>
            </div>
          }
        </header>
      </div>
    );
  }
}

export default App;
